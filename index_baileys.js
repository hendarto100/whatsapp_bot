import WA from '@adiwajshing/baileys'
import * as fs from 'fs'
import {
    createRequire
} from 'module';
const require = createRequire(
    import.meta.url);


const {
    WAConnection,
    MessageType,
    MessageOptions,
    Mimetype
} = WA


async function connectToWhatsApp() {
    const conn = new WAConnection()

    // this will be called as soon as the credentials are updated
    conn.on('credentials-updated', () => {
        // save credentials whenever updated
        console.log(`credentials updated!`)
        const authInfo = conn.base64EncodedAuthInfo() // get all the auth info we need to restore this session
        fs.writeFileSync('./auth_info.json', JSON.stringify(authInfo, null, '\t')) // save this info to a file
    })

    try {
        conn.loadAuthInfo('./auth_info.json')
    } catch (error) {
        //...
    }

    // menarik data dari csv
    const csv = require('csv-parse')
    const getStream = require('get-stream');

    const results = [];

    const csv_data = await new Promise((resolve, reject) => {
        fs.createReadStream('contact.csv')
            .pipe(csv({
                delimiter: ';'
            }))
            .on('data', (data) => results.push(data))
            .on('end', () => {
                resolve(results)
            });
    })

    //connect ke whatsapp
    await conn.connect()

    console.log('csv', csv_data)

    for (let i = 1; i < results.length; i++) {
        //console.log('hasil',results[i])

        setTimeout(async () => {

            try {
                const sentMsg = await conn.sendMessage(`${results[i][1]}@s.whatsapp.net`,
                    `oh hello there ur pass ${results[i][3]}\n
                        with this email ${results[i][2]}`, MessageType.text)
                console.log("sent message with ID '" + sentMsg.key.id + "' successfully to " + results[i][0])
            } catch (error) {
                console.log(error)
            }

        }, 1 * 1000)

    }

    /* example of custom functionality for tracking battery */
    conn.on('CB:action,,battery', json => {
        const batteryLevelStr = json[2][0][1].value
        const batterylevel = parseInt(batteryLevelStr)
        console.log('battery level: ' + batterylevel)
    })
    conn.on('close', ({
        reason,
        isReconnecting
    }) => (
        console.log('oh no got disconnected: ' + reason + ', reconnecting: ' + isReconnecting)
    ))

}
// run in main file

connectToWhatsApp()
    .catch(err => console.log("unexpected error: " + err)) // catch any errors
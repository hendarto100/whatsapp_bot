# Whatsapp Bot Bulk Send Messages

This project has build to send bulk chat into recipent by contact that saved in .csv format

## License
[MIT](https://choosealicense.com/licenses/mit/)
Copyright (c) 2021 ikohk

## Installation

Install yarn or npm first on your computer dekstop

```bash
yarn install
```

```bash
npm install
```

## Usage

Go to your root folder and send this command for using fast bot
```bash
yarn dev
```
Go to your root folder and send this command using slow bot, build based on puppeter

```bash
yarn start
```
after that scan qr code that show in cli
## Important

```bash
contact.csv
```
this file is used for ```yarn dev```, you can fill it with contact number and email that you have, fyi if you using slow bot i havent finishing yet for ```yarn start ```command so you need to hard code in ```index.js``` for filling contact number

if ```401``` code show up try to deleting file below

```bash
auth_info.json
```
this will trigger qr code to show up and you can scan it again to log in

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## Note

This library was originally a project for CS-2362 at Ashoka University and is in no way affiliated with WhatsApp. Use at your own discretion. Do not spam people with this.

this is an unofficial solution. It's not recommended using this in your company or for marketing purpose.
